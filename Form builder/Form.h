//
//  Form.h
//  Form builder
//
//  Created by zufa on 27/7/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FormElement.h"

@interface Form : UIView <UITextFieldDelegate>

@property  float spaceBetweenElements;

-(void) addFormElement:(FormElement *) element;
-(void) addToView:(UIView *)view atFrame:(CGRect)trame;
+(Form *)initWithStandardSetOfElements;
-(void)fliedResignFirstResponder:(UITextView *)textView;
-(void)fliedBecomeFirstResponder:(UITextView *)textView;
-(void)scrollToTextView:(UITextView *)textView;
-(void)setFliedIndent:(float)ident;
-(void)AddObservableFormElevent:(FormElement *)fornElemrnt;



@end