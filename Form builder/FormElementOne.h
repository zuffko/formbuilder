//
//  FormElementTwo.h
//  Form builder
//
//  Created by zufa on 28/7/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormElementOne : UIView <UITextViewDelegate>

@property (nonatomic, retain) UILabel *captionLabel;
@property (nonatomic, retain) UITextView *textView1;

- (void)drawForm;
- (float)heightOfFormElement: (FormElementOne* )element;
- (void)textViewDidBeginEditing:(UITextView *)textView;
- (void)textViewDidEndEditing:(UITextView *)textView;
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
@end
