//
//  FormElementTwo.h
//  Form builder
//
//  Created by zufa on 28/7/13.
//  Copyright (c) 2013 zufa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormElement : UIView <UITextViewDelegate>

@property (nonatomic, retain) UILabel *captionLabel;
@property (nonatomic, retain) UITextView *textView1;

-(float)formElementHeight;
-(void)setPlaceholder:(NSString *)placeholder forTextView:(UITextView *)textView;
-(void)setCaptionLabel:(UILabel *)captionLabel;

@end
